const TailwindCSS = require('tailwindcss')

module.exports = {
  syntax: 'postcss-scss',
  plugins: [
    TailwindCSS('config/tailwind.js'),
  ],
}