const merge  = require('webpack-merge')
const {common, commonStyles} = require('./webpack.common.js')

module.exports = merge(common, {

  /* Development-only settings */

  mode: 'development',
  devServer: {
    contentBase: common.output.path,
  },
  devtool: 'eval-source-map',

  plugins: [],
  
  module: {
    rules: [
      ...commonStyles(['style-loader']),
    ],
  },

})