const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

const root = path.resolve(__dirname, '..')
const src = path.resolve(root, 'src')
const dist = path.resolve(root, 'dist')
const res = path.resolve(root, 'res')
const config = path.resolve(root, 'config')

const paths = {root, src, dist, res, config}

const common =  {

  /* Global settings */
  context: root,
  entry: `${src}/client/www/main.ts`,

  output: {
    path: dist,
    filename: '[name]-[chunkhash:8].js'
  },

  resolve: {
    alias: {
      res: path.resolve(root, 'res'),
      www: path.resolve(src, 'client', 'www'),
      rust: path.resolve(root, 'pkg'),
    }
  },

  optimization: {
    noEmitOnErrors: true,
  },

  plugins: [

    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(src, 'client', 'www', 'appshell.html'),
    }),

    new CopyWebpackPlugin([
      `${res}/**/*.png`,
      `${res}/**/*.jpg`,
      `${res}/**/*.svg`,
    ]),

    new CleanWebpackPlugin([dist], {
      root: root,
    }),
  ],

  module: {
    rules: [
      {
        test: /\.(png)$/,
        use: ['file-loader']
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },

  resolve: {
    alias: {
      "@src": src,
      "@client": `${src}/client`,
      "@common": `${src}/common`,
      "@server": `${src}/server`,
      "@www": `${src}/client/www`,
      "@res": res,
    },
    extensions: ['.js', '.jsx', '.ts', '.tsx', '.json'],
  },
}

const commonStyles = (moreLoaders) => {
  moreLoaders = moreLoaders || []
  return [
    {
      test: /\.scss$/,
      use: [
        ...moreLoaders,
        'css-loader',
        'sass-loader',
        {
          loader: 'postcss-loader',
          options: {config: {path: config}},
        },
      ]
    },
    {
      test: /\.css$/,
      use: [
        ...moreLoaders,
        'css-loader',
        {
          loader: 'postcss-loader',
          options: { config: { path: config } },
        },
      ]
    },
  ]
}

module.exports = {paths, common, commonStyles}