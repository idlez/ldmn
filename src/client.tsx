import * as React from 'react';
import * as ReactDOM from 'react-dom';

import 'animate.css';

import { Battle } from './common/battle';
import { Monster } from './common/monster';
import { Adargana } from './common/monster/adargana_family';
import { Cindigre } from './common/monster/cindigre_family';
import { AppRoot } from './client/comps/app_root';
import { World } from './server';

export function mount(appEl: Element) {

  // Is this thing on?
  console.log("Initializing logic...")

  const testBattle = new Battle(new Monster(Adargana), new Monster(Cindigre))
  const world = new World(testBattle)

  // And mount the client view
  ReactDOM.render(<AppRoot world={world} />, appEl)
}
