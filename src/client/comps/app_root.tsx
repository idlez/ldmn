import * as React from 'react'
import { World } from "../../server"
import { BattleView } from "./battle_view"
import { Battle } from '@src/common/battle';
import { Action } from '@src/common/skill';

export interface AppRootProps {
  world: World,
}
interface AppRootState {
  world: World,
}
export class AppRoot extends React.Component<AppRootProps, AppRootState> {
  
  constructor(props: AppRootProps) {
    super(props)
    this.state = {world: props.world}
  }

  actionHandler = (act: Action<any, any, Battle>) => {
    this.setState(state => {
      state.world.battle.submitAction(act)
      return {world: state.world}
    })
  }

  render(): JSX.Element {
    return (
      <app-main class="flex-grow">
        {/* <header className="flex-none">--LDMN--</header> */}
        <main className="flex-grow">
          <BattleView battle={this.state.world.battle} onAction={this.actionHandler} />
        </main>
        {/* <footer className="flex-none">©idlez</footer> */}
      </app-main>
    )
  }

}