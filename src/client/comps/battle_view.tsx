import * as React from 'react';
import { CSSTransition } from 'react-transition-group';
import { Battle, BTrack, BTick } from '@src/common/battle';
import { MonsterDisplay, MonsterDetailed } from './monster_display';
import { Monster } from '@src/common/monster';
import { Color } from '@src/common/util/color';
import { range } from '@src/common/util/array';
import { Aspect } from '@src/common/aspect';
import { assertNever } from '@src/common/util/types';
import { Action } from '@src/common/skill';
import { AttackAction } from '@src/common/skills/attack';

interface DetailsContextI {
  showingDetails: boolean
  curDetails: BDetails
  showDetails(details: BDetails | boolean): void
}
export const DetailsContext = React.createContext<DetailsContextI>({
  showingDetails: false,
  curDetails: {detailsType: 'Empty'},
  showDetails: (_details: BDetails | boolean) => {},
})

interface BattleViewProps {
  battle: Battle
  onAction: (action: Action<any, any, Battle>) => void
}
interface BattleViewState {
  isShowingDetails: boolean
  showingDetailsOf: BDetails
}
export class BattleView extends React.Component<BattleViewProps, BattleViewState> {

  constructor(props: BattleViewProps) {
    super(props)
    this.state = {
      isShowingDetails: false,
      showingDetailsOf: {detailsType: "Empty"},
    }
  }

  setDetails = (details: BDetails | boolean) => {
    if (typeof details === 'boolean') {
      this.setState(() => {return {isShowingDetails: details}})
    } else {
      this.setState(() => {return {
        isShowingDetails: details.detailsType !== 'Empty',
        showingDetailsOf: details,
      }})
    }
  }

  render() {
    const {battle, onAction} = this.props
    return (
      <DetailsContext.Provider value={{showingDetails: this.state.isShowingDetails, curDetails: this.state.showingDetailsOf, showDetails: this.setDetails}}>
        <battle-view
          class="p-1 flex-grow flex-col border border-red rounded bg-grey-darker">
          <b-timeline class="flex-col flex-no-shrink">
            {battle.timeline.tracks.map(track => {
              return <TrackView
                key={track.actor.getStringId()}
                track={track}
                windowSize={100}
              />
            })}
          </b-timeline>
          <monsters-container class="flex-row flex-no-shrink content-around">
            {battle.participants.map(mon =>
              <div key={mon.getStringId()} className="flex-grow">
                <MonsterDisplay monster={mon} />
              </div>
            )}
          </monsters-container>
          <BattleLog log={battle.log} />
          <BDetailsPane
            details={this.state.showingDetailsOf}
            onAction={onAction}
          />
          <controls-container class="flex-col flex-no-shrink border-solid border-black">
            {battle.processUntilNeedsAction().map(([mon, acts]) => {
              return <div key={mon.getStringId()} className="flex-col flex-no-shrink">
                <p>Select {mon.displayName()}'s action</p>
                {acts.length !== 0
                  ? <BattleControls actions={acts} onAction={onAction} />
                  : <p>No actions available</p>}
              </div>
            })}
          </controls-container>
        </battle-view>
      </DetailsContext.Provider>
    )
  }
}

interface BattleLogProps {
  log: string[],
}
class BattleLog extends React.Component<BattleLogProps, any> {

  scrollLocked: boolean
  ref: React.RefObject<HTMLElement>

  constructor(props: BattleLogProps) {
    super(props)
    this.scrollLocked = true
    this.ref = React.createRef()
  }

  render() {
    return (
      <battle-log
        ref={this.ref}
        class="flex-col flex-grow overflow-y-auto break-normal border m-1"
        onScroll={this.handleScroll}
      >
        {this.props.log.map(
          (line, index) => <log-line key={index}>{line}<hr/></log-line>
        )}
      </battle-log>
    )
  }

  handleScroll = () => {
    const el = this.ref.current
    if (el === null) return
    if (el.scrollTop > el.scrollHeight - el.clientHeight - 5) {
      /* Close to scrolled down */
      this.scrollLocked = true
    } else {
      /* Scrolled up */
      this.scrollLocked = false
    }
  }

  componentDidUpdate() {
    if (this.scrollLocked) {
      const el = this.ref.current
      if (el === null) return
      window.requestAnimationFrame(() => {
        const lastChild = el.lastElementChild
        if (lastChild !== null) lastChild.scrollIntoView()
      })
    }
  }

}

interface BattleControlsProps {
  actions: Action<any, any, Battle>[]
  onAction: (action: Action<any, any, Battle>) => void
}
function BattleControls({ actions, onAction }: BattleControlsProps): JSX.Element {
  return (
    <DetailsContext.Consumer>
      {({curDetails, showDetails}) => (
        <battle-controls class="flex-wrap">
          {actions.map(act => {
            const clickHandler = () => {
              if (curDetails.detailsType === 'Action' && curDetails.detailsOf.uuid.equals(act.uuid)) {
                showDetails(false)
                onAction(act)
              } else {
                if (act instanceof AttackAction) {
                  showDetails({
                    detailsType: "Action",
                    actionType: 'Attack',
                    detailsOf: act,
                  })
                } else {
                  showDetails({
                    detailsType: "Action",
                    actionType: 'Plain',
                    detailsOf: act,
                  })
                }
              }
            }
            return <button key={act.name} data-desc={act.desc} onClick={clickHandler}>{act.name}<br /></button>
          })}
        </battle-controls>
      )}
    </DetailsContext.Consumer>
  )
}

interface TrackViewProps {
  track: BTrack<Monster>
  windowSize: number
}
function TrackView({ track, windowSize }: TrackViewProps): JSX.Element {
  const historySize = Math.floor(windowSize / 2)
  const futureSize = windowSize - historySize
  const start = track.curTime - historySize
  const end = track.curTime + futureSize

  return (
    <b-track>
      <DetailsContext.Consumer>
        {({showDetails}) =>
          range(start, end)
            .map(index => [track.ticks[index], index] as ([BTick<Monster> | undefined, number]))
            .filter(([tick, index]) => index === start || tick === undefined || tick.offset === 0) // include the first visible tick, all empty ticks, all ticks that are the first of their action phase
            .map(([tick, index]) => {
              if (tick === undefined) {
                return <b-tick
                  class={[track.curTime === index ? "cur-tick " : ""].join(' ')}
                  key={index << 16}
                ></b-tick>
              }
              const bgColor = Color.fromStringHash(tick.forAction.name)
              switch (tick.tickType) {
                case 'Windup': bgColor.brighten(20); break;
                case 'Cooldown': bgColor.darken(20); break;
              }

              const shownTicks = tick.totalTicks - tick.offset
              const flexGrow = Math.min(end - index, index + shownTicks - start, shownTicks)

              return <b-tick
                key={index << 16 | flexGrow}
                class="cursor-pointer"
                style={{
                  backgroundColor: bgColor.toString(),
                  flexGrow,
                }}
                data-desc={`${tick.forAction.name} ${tick.tickType} ${tick.totalTicks}`}
                onClick={() => {
                  showDetails({
                    detailsType: "Tick",
                    detailsOf: tick
                  })
                }}
              >
                <span
                  className="truncate text-center align-middle"
                  title={`${tick.forAction.name} ${tick.tickType}`}
                >
                  {/* {`${tick.forAction.name} ${tick.tickType}`} */}
                </span>
              </b-tick>
            })
        }
      </DetailsContext.Consumer>
    </b-track>
  )
}

interface BDetailsEmpty {
  detailsType: "Empty"
}
interface BDetailsTick {
  detailsType: "Tick"
  detailsOf: BTick<Monster>
}
interface BDetailsAspect {
  detailsType: "Aspect"
  detailsOf: Aspect<Monster>
}

interface BDetailsActionPlain {
  detailsType: "Action"
  actionType: "Plain"
  detailsOf: Action<any, any, Battle>
}
interface BDetailsActionAttack {
  detailsType: "Action"
  actionType: "Attack"
  detailsOf: AttackAction<Monster>
}
type BDetailsAction = BDetailsActionPlain | BDetailsActionAttack

interface BDetailsMonster {
  detailsType: "Monster"
  detailsOf: Monster
}
export type BDetails = BDetailsAction | BDetailsAspect | BDetailsEmpty | BDetailsTick | BDetailsMonster

interface BDetailsPaneProps {
  details: BDetails
  onAction: (action: Action<Monster, any, Battle>) => void
}
function BDetailsPane({details, onAction}: BDetailsPaneProps): JSX.Element {
  return (
    <DetailsContext.Consumer>
      {({showDetails, showingDetails}) => {
        let inner: JSX.Element | JSX.Element[] | undefined
        switch (details.detailsType) {
          case 'Empty': {
            inner = []
          } break
          case 'Tick': {
            const tick = details.detailsOf
            const act = tick.forAction
            inner = <div className="flex-col">
              <span>{act.name}</span><br/>
              <span>{act.desc}</span><br/>
              <span>{tick.tickType}: {tick.totalTicks} beats</span>
            </div>
          } break
          case 'Aspect': {
            const asp = details.detailsOf
            inner = <div className="flex-col">
              <span>{asp.enabled ? "Enabled" : "Disabled"}</span><br />
              <span>{asp.name}</span><br />
              <span>{asp.desc}</span><br />
            </div>
          } break
          case 'Action': {
            let actionTypeSpecific: JSX.Element = <></>
            switch (details.actionType) {
              case 'Plain': {
                // no specific data to show
              } break
              case 'Attack': {
                const act = details.detailsOf
                // show target, cost, damage spread
                actionTypeSpecific = <div className="flex-col p-1">
                  <span>Target: {act.target.displayName()}</span><br/>
                  <span>Cost: {act.cost.toString()}</span><br/>
                  <span>Power: {act.powers.toString()}</span><br/>
                </div>
              } break
              default: assertNever(details)
            }
            const act = details.detailsOf
            inner = <div className="flex-col w-full">
              <div className="flex-row">
                <div className="flex-col p-1">
                  <span>{act.name}</span><br />
                  <span>{act.desc}</span><br />
                  {act.duration.pre  === 0 ? <></> : <><span>Windup: { act.duration.pre } beats</span><br /></>}
                  {act.duration.main === 1 ? <></> : <><span>Impact: { act.duration.main} beats</span><br /></>}
                  {act.duration.post === 0 ? <></> : <><span>Stagger: {act.duration.post} beats</span><br /></>}
                </div>
                {actionTypeSpecific}
              </div>
              <div className="flex-row">
                <button
                  onClick={() => {
                    showDetails(false)
                    onAction(act)
                  }}
                >DO IT</button>
                <button
                  onClick={() => showDetails(false)}
                >WAIT DON'T</button>
              </div>
            </div>
          } break
          case 'Monster': {
            const mon = details.detailsOf
            inner = <MonsterDetailed profileHeight={10} monster={mon} />
          } break
          default: assertNever(details)
        }
        return (
          <CSSTransition
            in={showingDetails}
            timeout={300}
            classNames="details"
            unmountOnExit
            onExited={() => {
              showDetails({detailsType: "Empty"})
            }}
          >
            <details-pane class="relative overflow-hidden border">
              <button
                className={["x-button", "mx-4", details.detailsType === 'Empty' ? "hidden" : ""].join(' ')}
                onClick={() => showDetails(false)}
              >X</button>
              <div className="overflow-auto w-full">
                {inner}
              </div>
            </details-pane>
          </CSSTransition>
        )

      }}
    </DetailsContext.Consumer>
  )
}
