import * as React from 'react';
import { Monster, StatBoost, Energies, EnergyType } from '@src/common/monster';
import { Aspect } from '@src/common/aspect';
import { BDetails, DetailsContext } from './battle_view';
import { Uuid } from '@src/common/util/uuid';
import { render } from 'react-dom';
import { makeListener, TookDamageEvent, TookDamageListener } from '@src/common/event';
import { animateCss } from '../util/anim';

interface MonsterDisplayProps {
  monster: Monster
  profileHeight?: number
}
export class MonsterDisplay extends React.Component<MonsterDisplayProps, MonsterDisplayProps> {

  damageListener?: TookDamageListener<Monster>
  shouldAnimateDamage: boolean = false
  imgRef: React.RefObject<HTMLImageElement>

  constructor(props: MonsterDisplayProps) {
    super(props)
    this.state = {...props}
    this.imgRef = React.createRef()
  }

  componentDidMount() {
    this.damageListener = makeListener<Monster, TookDamageListener<Monster>>(
      'TookDamage',
      (ev: TookDamageEvent<Monster>) => {
        this.shouldAnimateDamage = true
      }
    )
    this.state.monster.addEventListener(this.damageListener)
  }

  componentWillUnmount() {
    if (this.damageListener !== undefined) {
      this.state.monster.removeEventListener(this.damageListener)
      this.damageListener = undefined
    }
  }
  
  render(): JSX.Element {
    const curEnr = this.state.monster.energy()
    const maxEnr = this.state.monster.maxEnergy()

    if (this.shouldAnimateDamage) {
      const el = this.imgRef.current
      if (el !== null) {
        animateCss(el, 'rubberBand')
      }
      this.shouldAnimateDamage = false
    }

    return (
      <monster-display class="flex-col w-full h-full">
        <DetailsContext.Consumer>
          {({showDetails}) => <>
            <figure className="flex-col">
              <img
                ref={this.imgRef}
                src={this.state.monster.profileImg()}
                alt={"A portrait of " + this.state.monster.displayName()}
                style={this.state.profileHeight ? {height: this.state.profileHeight + "rem"} : {}}
                className="p-4"
              />
              <figcaption
                className="cursor-pointer"
                onClick={() => {showDetails({detailsType: 'Monster', detailsOf: this.state.monster})}}
              >{this.state.monster.displayName()}</figcaption>
            </figure>
            <EnergiesPane currentEnergies={curEnr} maxEnergies={maxEnr} />
            <AspectsPane aspects={this.state.monster.getAspects()} />
            <StatBoostDisplay boosts={this.state.monster.getStatBoosts()} />
          </>}
        </DetailsContext.Consumer>
      </monster-display>
    )
  }
}

interface MonsterDetailedProps {
  monster: Monster
  profileHeight?: number
}
export function MonsterDetailed({ monster, profileHeight }: MonsterDetailedProps): JSX.Element {
  const curEnr = monster.energy()
  const maxEnr = monster.maxEnergy()
  return (
    <monster-detailed class="flex-row w-full h-full">
      <DetailsContext.Consumer>
        {({showDetails}) => <>
          <figure className="flex-col">
            <img
              src={monster.profileImg()}
              alt={"A portrait of " + monster.displayName()}
              style={profileHeight ? {height: profileHeight + "rem"} : {}}
            />
            <figcaption>
              {monster.displayName()}
              <sub>{monster.getSpecies().name}</sub>
            </figcaption>
          </figure>
          <div className="flex-col flex-wrap content-start">
            <EnergiesPane currentEnergies={curEnr} maxEnergies={maxEnr} />
            <AspectsPane aspects={monster.getAspects()} />
            <StatBoostDisplay boosts={monster.getStatBoosts()} />
          </div>
        </>}
      </DetailsContext.Consumer>
    </monster-detailed>
  )
}

interface EnergiesPaneProps {
  currentEnergies: Energies
  maxEnergies: Energies
}
function EnergiesPane({currentEnergies, maxEnergies}: EnergiesPaneProps): JSX.Element {
  const curEnr = currentEnergies
  const maxEnr = maxEnergies
  return (
    <div className="energies-pane"
      style={{
        display: 'grid',
        gridTemplateColumns: 'auto 1fr',
      }}
    >
      {[...curEnr.entries()].filter(([type]) => maxEnr.getWithoutDefault(type) !== undefined).map(([type, val]) => {
        return (
          <EnergyMeter
            key={type}
            energyType={type} current={val} maximum={maxEnr.get(type)}
          />
        )
      })}
    </div>
  )
}

interface EnergyMeterProps {
  energyType: EnergyType
  current: number
  maximum: number
}
function EnergyMeter({energyType, current, maximum}: EnergyMeterProps): JSX.Element {
  const rCur = Math.round(current)
  const rMax = Math.round(maximum)
  const ratio = current/maximum
  const stringDisp = `${rCur}/${rMax}`
  const emptyColor = 'black'
  const fillColor = 'green'
  return (<>
    <div className="p-1 text-xs"
      style={{
        gridColumn: '1'
      }}
    >
      <span>{energyType}:</span>
    </div>
    <energy-meter class="w-full flex-grow m-px text-xs"
      style={{
        gridColumn: '2',
        backgroundColor: emptyColor,
      }}
      title={stringDisp}
      data-overlay={stringDisp}
    >
      <span style={{
        width: `${ratio*100}%`,
        backgroundColor: fillColor
      }}></span>
    </energy-meter>
  </>)
}

interface AspectsPaneProps {
  aspects: Aspect<Monster>[]
}
function AspectsPane({ aspects }: AspectsPaneProps): JSX.Element {
  const importants = aspects.filter(a => a.important)
  const enableds = importants.filter(a => a.enabled)
  const disableds = importants.filter(a => !a.enabled)

  return (
    <aspects-pane class="flex-wrap">
      <DetailsContext.Consumer>
        {({showDetails}) => <>
          {enableds.map(a => <AspectDisplay key={a.name} aspect={a} onDetails={showDetails} />)}
          {disableds.map(a => <AspectDisplay key={a.name} aspect={a} onDetails={showDetails} />)}
        </>}
      </DetailsContext.Consumer>
    </aspects-pane>
  )
}

interface AspectDisplayProps {
  aspect: Aspect<Monster>
  onDetails: (details: BDetails) => void
}
function AspectDisplay({ aspect, onDetails }: AspectDisplayProps): JSX.Element {
  return (
    <aspect-el
      data-desc={(aspect.enabled ? "Active: " : "Inactive: ") + aspect.desc}
      class={[aspect.enabled ? "" : "disabled", "cursor-pointer"].join(' ')}
      onClick={(ev: MouseEvent) => {
        if (ev.target instanceof HTMLElement) {
          ev.target.focus()
        }
        onDetails({
          detailsType: 'Aspect',
          detailsOf: aspect
        })
      }}
    >
      {aspect.name}<br/>
    </aspect-el>
  )
}

interface StatBoostDisplayProps {
  boosts: [Uuid, StatBoost][],
}
function StatBoostDisplay({boosts}: StatBoostDisplayProps): JSX.Element {
  return (
    <stat-boost-list class="flex-col">
      {boosts.map(([id, boost]) => {
        let type, stats
        if (boost.statBoostType === 'additive') {
          type = "+"
          stats = boost.statDiff
        } else {
          type = "×"
          stats = boost.statMultiplier
        }
        return <stat-boost key={id}>{type}{JSON.stringify(stats)}</stat-boost>
      })}
    </stat-boost-list>
  )
}
