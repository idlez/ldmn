import '@www/base.scss'

import * as Client from '@src/client'

const appRoot = document.getElementById('app-root')
if (appRoot !== null) {
  Client.mount(appRoot)
} else {
  console.error("Failed to find #app-root element")
}