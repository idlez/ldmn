import { assertNever, Omit } from './util/types';
import { Battle } from './battle';
import { DamageSpread, DamageType, DamageSpreadI } from "./damage";
import { Aspect } from './aspect';

import { NEUTRAL_STAT_VALUE } from './constant';
import { LDEventEmitter, LDListener, AddedAspectListener, AddingAspectListener, ShouldCancel, RemovingAspectListener, EnablingAspectListener, DisablingAspectListener, RemovedAspectListener, EnabledAspectListener, DisabledAspectListener, TimePassedListener, TakingDamageListener, TookDamageListener, LosingEnergyListener, LostEnergyListener, EnergyLossCause, EnergyGainCause, GainingEnergyListener, LostEnergyEvent, GainingEnergyEvent, GainedEnergyEvent, GainedEnergyListener, LosingEnergyEvent, TookDamageEvent, TakingDamageEvent, TimePassedEvent, DisabledAspectEvent, DisablingAspectEvent, EnabledAspectEvent, EnablingAspectEvent, RemovedAspectEvent, RemovingAspectEvent, AddedAspectEvent, AddingAspectEvent } from './event';
import { Uuid } from './util/uuid';
import { MapDefault } from './util/map_default';
import { Action, Skill } from './skill';

export interface StatBoostAdditive {
  statBoostType: 'additive'
  priority: 'beforeMultiplicative' | 'afterMultiplicative'
  statDiff: MonStatsI
}
export interface StatBoostMultiplicative {
  statBoostType: 'multiplicative'
  statMultiplier: MonStatsI
}

export function makeStatBoost(stats: MonStatsI, priority: 'before' | 'after'): StatBoostAdditive
export function makeStatBoost(stats: MonStatsI): StatBoostMultiplicative
export function makeStatBoost(stats: MonStatsI, priority?: 'before' | 'after'): StatBoostAdditive | StatBoostMultiplicative {
  if (priority === undefined) {
    return {
      statBoostType: 'multiplicative',
      statMultiplier: stats,
    }
  }
  else {
    switch (priority) {
      case 'before': return {
        statBoostType: 'additive',
        statDiff: stats,
        priority: 'beforeMultiplicative',
      }
      case 'after':  return {
        statBoostType: 'additive',
        statDiff: stats,
        priority: 'afterMultiplicative',
      }
      default: return assertNever(priority)
    }
  }
}
export type StatBoost = StatBoostAdditive | StatBoostMultiplicative

export class Monster implements LDEventEmitter<Monster> {
  private uuid: Uuid
  private nick?: string
  private species: MonSpecies
  private statMods: MonStats
  private statBoosts: [Map<Uuid, StatBoostAdditive>, Map<Uuid, StatBoostMultiplicative>, Map<Uuid, StatBoostAdditive>]
  private energyMods: Energies
  private curEnergy: Energies
  private aspects: Map<Uuid, Aspect<Monster>> = new Map()
  private eventListeners: Map<LDListener<Monster>["listenerType"], Set<LDListener<Monster>>> = new Map()
  private incapacitatingFactors: Map<Uuid, Aspect<Monster>>
  private skills: Map<string, Skill>

  private uuidString: string

  constructor(species: MonSpecies) {
    this.uuid = new Uuid()
    this.nick = undefined
    this.species = species
    this.statMods = new MonStats()
    this.statBoosts = [new Map(), new Map(), new Map()]
    this.energyMods = new Energies({})
    this.curEnergy = this.maxEnergy()
    this.incapacitatingFactors = new Map()
    for (const aspect of species.aspects) {
      this.addAspect(aspect)
    }
    this.skills = new Map()
    for (const skill of species.skills) {
      this.skills.set(skill.name, skill)
    }

    this.uuidString = this.uuid.toString()
  }

  isIncapacitated(): boolean {
    return this.incapacitatingFactors.size !== 0
  }

  addIncapacitatingFactor(factor: Aspect<Monster>) {
    this.incapacitatingFactors.set(factor.uuid, factor)
  }

  removeIncapactiatingFactor(factorId: Uuid) {
    this.incapacitatingFactors.delete(factorId)
  }

  getId(): Uuid {
    return this.uuid
  }

  getStringId(): string {
    return this.uuidString
  }

  getAspects(): Aspect<Monster>[] {
    return [...this.aspects.values()]
  }

  getStatBoosts(): [Uuid, StatBoost][] {
    const [pre, mul, post] = this.statBoosts
    return [...pre.entries(), ...mul.entries(), ...post.entries()]
  }

  /**
   * This isn't a lookup, it's a bit expensive.
   * Prefer caching to repeated calls.
   */
  calcEffectiveStats(): MonStats {
    const [preAdd, mult, postAdd] = this.statBoosts
    const stats = this.species.baseStats.add(this.statMods)

    for (const {statDiff: addedStats} of preAdd.values()) {
      stats.addAssign(addedStats)
    }
    for (const {statMultiplier: multipliedStats} of mult.values()) {
      stats.multAssign(multipliedStats)
    }
    for (const {statDiff: addedStats} of postAdd.values()) {
      stats.addAssign(addedStats)
    }

    return stats
  }

  maxEnergy(): Energies {
    return this.species.baseEnergy.add(this.energyMods)
  }

  energy(): Energies {
    return this.curEnergy
  }

  profileImg(): string {
    return this.species.assets.profileImg
  }

  displayName(): string {
    return this.nick || this.species.name
  }

  getSpecies(): MonSpecies {
    return this.species
  }

  // TODO: Make this *less* generic in the future, once I know what contexts exist
  // TODO: Also don't hardcode this. Store action factories on Monster and generate
  //       valid Actions when this is called.
  actionsIn<C>(ctx: C): Action<this, any, C>[] {
    // Only handle Battle context for now
    if (ctx instanceof Battle) {
      return [...this.skills.values()].reduce((actions, skill) => {
        actions.push(...skill.actionsFor(this, ctx))
        return actions
      }, new Array())
    }
    return []
  }

  /**
   * Applies this Monster's Aspect modifiers without actually losing energy
   * @param cost Energy to lose before this Monster's Aspect modifiers
   * @param cause The reason this energy is being lost
   */
  predictEnergyCost(cost: EnergiesI, cause: EnergyLossCause): EnergiesI {
    return this.emitLosingEnergy(cost, cause)
  }

  /**
   * Convenience method equivalent to Monster.energy().sub(Monster.predictEnergyCost(loss, cause))
   * @param loss Energy to lose before this Monster's Aspect modifiers
   * @param cause The reason this energy is being lost
   */
  predictRemainingEnergy(loss: EnergiesI, cause: EnergyLossCause): Energies {
    loss = this.emitLosingEnergy(loss, cause)
    return this.curEnergy.sub(loss)
  }

  /**
   * Drain energy from this monster. Called to expend resources and as part of damage application.
   * @param loss Energy to lose before this Monster's Aspect modifiers
   * @param cause The reason this energy is being lost
   */
  drainEnergy(loss: EnergiesI, cause: EnergyLossCause) {
    loss = this.emitLosingEnergy(loss, cause)
    this.curEnergy.subAssign(loss)
    this.emitLostEnergy(loss, cause)
    return true
  }

  /**
   * Restore energy to this monster. Called to heal or regain resources.
   * @param gain Energy to gain before this Monster's Aspect modifiers
   * @param cause The reason this energy is being lost
   */
  restoreEnergy(gain: EnergiesI, cause: EnergyGainCause) {
    gain = this.emitGainingEnergy(gain, cause)
    this.curEnergy.addAssign(gain)
    this.emitGainedEnergy(gain, cause)
    return true
  }

  // TODO: Give each individual species/mon a map for this
  offensiveStatFor(type: DamageType): number {
    switch (type) {
      // Physical
      case DamageType.Slash:
      case DamageType.Pierce:
      case DamageType.Blunt:
      case DamageType.Crush:
      case DamageType.Vibration: // Maybe...
        return this.calcEffectiveStats().str
      // Elemental
      case DamageType.Heat:
      case DamageType.Cold:
      case DamageType.Zap:
      case DamageType.Light:
      case DamageType.Dark:
      // Special
      case DamageType.Acid:
      case DamageType.Toxic:
      case DamageType.Decay:
      case DamageType.Mental:
      case DamageType.Force:
        return NEUTRAL_STAT_VALUE
      case DamageType.Exertion:
        console.error(`[Monster] offensiveStatFor(${type}) is invalid: can't deal damage of this type`)
        return -Infinity
    }
  }

  // TODO: Give each individual species/mon a map for this
  defensiveStatFor(type: DamageType): number {
    let ret
    switch (type) {
      // Physical
      case DamageType.Slash:
      case DamageType.Pierce:
      case DamageType.Blunt:
      case DamageType.Crush:
      case DamageType.Vibration:
        ret = this.calcEffectiveStats().frt
        break
      // Elemental
      case DamageType.Heat:
      case DamageType.Cold:
      case DamageType.Zap:
      case DamageType.Light:
      case DamageType.Dark:
      // Special
      case DamageType.Mental:
        ret = this.calcEffectiveStats().wis
        break
      case DamageType.Acid:
      case DamageType.Toxic:
      case DamageType.Decay:
      case DamageType.Force:
        ret = NEUTRAL_STAT_VALUE
        break
      default:
        console.error(`[Monster] defensiveStatFor(${type}) Failed to match type to a known DamageType`, type, this)
        ret = -Infinity // this should wreck something
    }
    return ret + (this.species.baseResist[type] || 0)
  }

  // TODO: Mutate a 'statBoost: MonStats' in addition to storing boosts for faster lookup of computed effective stats
  // storing the boosts should allow neat UI
  addStatBoost(id: Uuid, boost: StatBoost) {
    switch (boost.statBoostType) {
      case 'additive':
        switch (boost.priority) {
          case 'beforeMultiplicative':
            this.statBoosts[0].set(id, boost)
            break
          case 'afterMultiplicative':
            this.statBoosts[2].set(id, boost)
            break
          default: assertNever(boost.priority)
        }
        break
      case 'multiplicative':
        this.statBoosts[1].set(id, boost)
        break
      default: assertNever(boost)
    }
  }
  removeStatBoost(id: Uuid) {
    for (const map of this.statBoosts) {
      map.delete(id)
    }
  }

  addEventListener(listener: LDListener<Monster>) {
    let evLs = this.eventListeners.get(listener.listenerType)
    if (evLs === undefined) {
      evLs = new Set()
      this.eventListeners.set(listener.listenerType, evLs)
    }

    evLs.add(listener)
  }

  removeEventListener(listener: LDListener<Monster>): boolean {
    let evLs = this.eventListeners.get(listener.listenerType)
    if (evLs === undefined) {
      return false
    }

    return evLs.delete(listener)
  }

  /**
   * @returns didAdd: boolean
   */
  addAspect(aspect: Aspect<Monster>): boolean {
    // ignore adds for an Aspect we already have. If you want to replace an
    // aspect, remove it first.
    if (this.aspects.has(aspect.uuid)) return false

    if (aspect.onAdding && aspect.onAdding(this)) return false
    if (this.emitAddingAspect(aspect)) return false

    // Register Aspect and listeners
    this.aspects.set(aspect.uuid, aspect)
    for (const listener of aspect.listeners || []) {
      this.addEventListener(listener)
    }

    if (aspect.onAdded) aspect.onAdded(this)
    this.emitAddedAspect(aspect)

    // Adding an enabled Aspect should fire enabling actions
    if (aspect.enabled) {
      // This is a bit non-trivial. When adding an enabled Aspect, that
      // means we have to check our Enabling listeners so they can object
      // and force the Aspect into a disabled state. The aspect will always
      // be added, but may not be enabled.
      if (aspect.onEnabling) aspect.enabled = !aspect.onEnabling(this)
      if (aspect.enabled) aspect.enabled = !this.emitEnablingAspect(aspect)
      if (aspect.enabled) {
        // And now enabled actions
        if (aspect.onEnabled) aspect.onEnabled(this)
        this.emitEnabledAspect(aspect)
      }
    }

    return true
  }

  /**
   * @returns didRemove boolean
   */
  removeAspect(aspectId: Uuid): boolean
  removeAspect(aspectName: string): boolean
  removeAspect(aspectId: Uuid | string): boolean {
    
    // Remove all aspects matching the given name
    if (typeof aspectId === 'string') {
      let anyRemoved = false
      for (const [id, aspect] of this.aspects.entries()) {
        if (aspect.name === aspectId && this.removeAspect(id)) anyRemoved = true
      }
      return anyRemoved
    }

    // Remove the aspect matching this uuid
    const aspect = this.aspects.get(aspectId)
    if (aspect === undefined) return false // not found

    if (aspect.onRemoving) return false
    if (this.emitRemovingAspect(aspect)) return false

    // Unregister Aspect and listeners
    this.aspects.delete(aspectId)
    for (const listener of aspect.listeners || []) {
      this.removeEventListener(listener)
    }

    // Removing an Aspect implies first disabling it
    // Note that Disabling events aren't triggered, only Disabled; you can't cancel disabling a removed Aspect.
    if (aspect.onDisabled) aspect.onDisabled(this)
    this.emitDisabledAspect(aspect)

    if (aspect.onRemoved) aspect.onRemoved(this)
    this.emitRemovedAspect(aspect)

    return true
  }

  /**
   * @returns nowEnabled
   */
  enableAspect(aspectId: Uuid): boolean 
  enableAspect(aspectName: string): boolean 
  enableAspect(aspectId: Uuid | string): boolean {

    // Enable all aspects matching the given name
    if (typeof aspectId === 'string') {
      let anyEnabled = false
      for (const [id, aspect] of this.aspects.entries()) {
        if (aspect.name === aspectId && this.enableAspect(id)) anyEnabled = true
      }
      return anyEnabled
    }

    // Enable the aspect matching this uuid
    const aspect = this.aspects.get(aspectId)
    if (aspect === undefined) return false // not found

    if (aspect.enabled) return true

    if (aspect.onEnabling && aspect.onEnabling(this)) return false
    if (this.emitEnablingAspect(aspect)) return false

    aspect.enabled = true

    if (aspect.onEnabled) aspect.onEnabled(this)
    this.emitEnabledAspect(aspect)

    return true
  }

  /**
   * @returns nowDisabled
   */
  disableAspect(aspectId: Uuid): boolean
  disableAspect(aspectName: string): boolean
  disableAspect(aspectId: Uuid | string): boolean {
    // Disable all aspects matching the given name
    if (typeof aspectId === 'string') {
      let anyDisabled = false
      for (const [id, aspect] of this.aspects.entries()) {
        if (aspect.name === aspectId && this.disableAspect(id)) anyDisabled = true
      }
      return anyDisabled
    }

    // Disable the aspect matching this uuid
    const aspect = this.aspects.get(aspectId)
    if (aspect === undefined) return true // gone is disabled, right?

    if (!aspect.enabled) return true

    if (aspect.onDisabling && aspect.onDisabling(this)) return false
    if (this.emitDisablingAspect(aspect)) return false

    aspect.enabled = false

    if (aspect.onDisabled) aspect.onDisabled(this)
    this.emitDisabledAspect(aspect)
    
    return true
  }

  private damageToEnergyLoss(damageType: DamageType, damageAmount: number): EnergiesI {
    let energyLoss: EnergiesI = {}

    // Default mapping
    switch (damageType) {
      case DamageType.Slash:
      case DamageType.Pierce:
      case DamageType.Blunt:
      case DamageType.Crush:
      case DamageType.Vibration:
      case DamageType.Heat:
      case DamageType.Cold:
      case DamageType.Zap:
      case DamageType.Light:
      case DamageType.Dark:
      case DamageType.Acid:
      case DamageType.Toxic:
      case DamageType.Decay:
        energyLoss.health = damageAmount
        break
      case DamageType.Mental:
        energyLoss.will = damageAmount
        break
      case DamageType.Force:
      case DamageType.Exertion:
        energyLoss.stam = damageAmount
        break
      default: assertNever(damageType)
    }

    return energyLoss
  }

  takeDamage(damage: DamageSpreadI) {

    damage = this.emitTakingDamage(damage)

    // Take each damage type
    const spread = new DamageSpread(damage)
    for (const [type, amount] of spread.entries()) {
      let loss = this.damageToEnergyLoss(type, amount)
      this.drainEnergy(loss, type)
    }

    this.emitTookDamage(damage)
  }

  /* Event Emitters/Listeners */

  emitAddingAspect(aspect: Aspect<Monster>): ShouldCancel {
    const event: AddingAspectEvent<Monster> = {
      eventType: 'AddingAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("AddingAspect") || []) {
      if ((listener as AddingAspectListener<Monster>)(event)) return true
    }
    return false
  }

  emitAddedAspect(aspect: Aspect<Monster>): void {
    const event: AddedAspectEvent<Monster> = {
      eventType: 'AddedAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("AddedAspect") || []) {
      (listener as AddedAspectListener<Monster>)(event)
    }
  }

  emitRemovingAspect(aspect: Aspect<Monster>): ShouldCancel {
    const event: RemovingAspectEvent<Monster> = {
      eventType: 'RemovingAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("RemovingAspect") || []) {
      if ((listener as RemovingAspectListener<Monster>)(event)) return true
    }
    return false
  }

  emitRemovedAspect(aspect: Aspect<Monster>): void {
    const event: RemovedAspectEvent<Monster> = {
      eventType: 'RemovedAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("RemovedAspect") || []) {
      (listener as RemovedAspectListener<Monster>)(event)
    }
  }

  emitEnablingAspect(aspect: Aspect<Monster>): ShouldCancel {
    const event: EnablingAspectEvent<Monster> = {
      eventType: 'EnablingAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("EnablingAspect") || []) {
      if ((listener as EnablingAspectListener<Monster>)(event)) return true
    }
    return false
  }

  emitEnabledAspect(aspect: Aspect<Monster>): void {
    const event: EnabledAspectEvent<Monster> = {
      eventType: 'EnabledAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("EnabledAspect") || []) {
      (listener as EnabledAspectListener<Monster>)(event)
    }
  }

  emitDisablingAspect(aspect: Aspect<Monster>): ShouldCancel {
    const event: DisablingAspectEvent<Monster> = {
      eventType: 'DisablingAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("DisablingAspect") || []) {
      if ((listener as DisablingAspectListener<Monster>)(event)) return true
    }
    return false
  }

  emitDisabledAspect(aspect: Aspect<Monster>): void {
    const event: DisabledAspectEvent<Monster> = {
      eventType: 'DisabledAspect',
      source: this,
      aspect,
    }
    for (const listener of this.eventListeners.get("DisabledAspect") || []) {
      (listener as DisabledAspectListener<Monster>)(event)
    }
  }

  emitTimePassed(delta: number): void {
    const event: TimePassedEvent<Monster> = {
      eventType: 'TimePassed',
      source: this,
      delta,
    }
    for (const listener of this.eventListeners.get("TimePassed") || []) {
      (listener as TimePassedListener<Monster>)(event)
    }
  }

  emitTakingDamage(damage: DamageSpreadI): DamageSpreadI {
    const event: Omit<TakingDamageEvent<Monster>, 'damage'> = {
      eventType: 'TakingDamage',
      source: this,
    }
    for (const listener of this.eventListeners.get("TakingDamage") || []) {
      damage = (listener as TakingDamageListener<Monster>)({damage, ...event})
    }
    return damage
  }

  emitTookDamage(damage: DamageSpreadI): void {
    const event: TookDamageEvent<Monster> = {
      eventType: 'TookDamage',
      source: this,
      damage,
    }
    for (const listener of this.eventListeners.get("TookDamage") || []) {
      (listener as TookDamageListener<Monster>)(event)
    }
  }

  emitLosingEnergy(energies: EnergiesI, cause: EnergyLossCause): EnergiesI {
    const event: Omit<LosingEnergyEvent<Monster>, 'energies'> = {
      eventType: 'LosingEnergy',
      source: this,
      cause,
    }
    for (const listener of this.eventListeners.get("LosingEnergy") || []) {
      energies = (listener as LosingEnergyListener<Monster>)({energies, ...event})
    }
    return energies
  }

  emitLostEnergy(energies: EnergiesI, cause: EnergyLossCause): void {
    const event: LostEnergyEvent<Monster> = {
      eventType: 'LostEnergy',
      source: this,
      energies,
      cause,
    }
    for (const listener of this.eventListeners.get("LostEnergy") || []) {
      (listener as LostEnergyListener<Monster>)(event)
    }
  }

  emitGainingEnergy(energies: EnergiesI, cause: EnergyGainCause): EnergiesI {
    const event: Omit<GainingEnergyEvent<Monster>, 'energies'> = {
      eventType: 'GainingEnergy',
      source: this,
      cause,
    }
    for (const listener of this.eventListeners.get("LosingEnergy") || []) {
      energies = (listener as GainingEnergyListener<Monster>)({energies, ...event})
    }
    return energies
  }

  emitGainedEnergy(energies: EnergiesI, cause: EnergyGainCause): void {
    const event: GainedEnergyEvent<Monster> = {
      eventType: 'GainedEnergy',
      source: this,
      energies,
      cause,
    }
    for (const listener of this.eventListeners.get("LostEnergy") || []) {
      (listener as GainedEnergyListener<Monster>)(event)
    }
  }

}

export interface MonSpecies {
  name: string
  family: MonFamily
  baseStats: MonStats
  baseEnergy: Energies
  baseResist: DamageSpreadI
  aspects: Aspect<Monster>[]
  assets: MonAssets
  skills: Skill[]
}

export interface MonFamily {
  name: string,
}

export interface MonAssets {
  profileImg: string,
}

export enum MonStatType {
  Constitution = "con",
  Fortitude = "frt",
  Strength = "str",
  Dexterity = "dex",
  Speed = "spd",
  Wisdom = "wis",
  Luck = "luk",
}
export type MonStatsI = {
  [T in MonStatType]?: number
}

export class MonStats implements MonStatsI {
  con: number = 0 // constitution
  frt: number = 0 // fortitude
  str: number = 0 // strength
  dex: number = 0 // dexterity
  spd: number = 0 // speed
  wis: number = 0 // wisdom
  luk: number = 0 // luck

  static fromI(src: MonStatsI): MonStats {
    let ret = new MonStats()
    Object.assign(ret, src)
    return ret
  }

  add(other: MonStatsI): MonStats {
    return MonStats.fromI({
      con: this.con + (other.con || 0),
      frt: this.frt + (other.frt || 0),
      str: this.str + (other.str || 0),
      dex: this.dex + (other.dex || 0),
      spd: this.spd + (other.spd || 0),
      wis: this.wis + (other.wis || 0),
      luk: this.luk + (other.luk || 0),
    })
  }

  sub(other: MonStatsI): MonStats {
    return MonStats.fromI({
      con: this.con - (other.con || 0),
      frt: this.frt - (other.frt || 0),
      str: this.str - (other.str || 0),
      dex: this.dex - (other.dex || 0),
      spd: this.spd - (other.spd || 0),
      wis: this.wis - (other.wis || 0),
      luk: this.luk - (other.luk || 0),
    })
  }

  addAssign(other: MonStatsI): MonStats {
    this.con += (other.con || 0)
    this.frt += (other.frt || 0)
    this.str += (other.str || 0)
    this.dex += (other.dex || 0)
    this.spd += (other.spd || 0)
    this.wis += (other.wis || 0)
    this.luk += (other.luk || 0)
    return this
  }

  multAssign(other: MonStatsI): MonStats {
    this.con *= (other.con || 1)
    this.frt *= (other.frt || 1)
    this.str *= (other.str || 1)
    this.dex *= (other.dex || 1)
    this.spd *= (other.spd || 1)
    this.wis *= (other.wis || 1)
    this.luk *= (other.luk || 1)
    return this
  }
  
  subAssign(other: MonStatsI): MonStats {
    this.con -= (other.con || 0)
    this.frt -= (other.frt || 0)
    this.str -= (other.str || 0)
    this.dex -= (other.dex || 0)
    this.spd -= (other.spd || 0)
    this.wis -= (other.wis || 0)
    this.luk -= (other.luk || 0)
    return this
  }

  statTotal(): number {
    return 0
      + this.con
      + this.frt
      + this.dex
      + this.str
      + this.spd
      + this.wis
      + this.luk
  }
}

export enum EnergyType {
  // status
  Stamina = "stam",
  Willpower = "will",
  Health = "health",
  // elemental
  Fire = "fire",
  Ice = "ice",
  Electricity = "elec",
  Light = "light",
  Dark = "dark",
}
export type EnergiesI = {
  [T in EnergyType]?: number
}

export class Energies extends MapDefault<EnergyType, number> implements EnergiesI {

  get stam() {return this.get(EnergyType.Stamina)}
  set stam(val) {this.set(EnergyType.Stamina, val)}
  get will() {return this.get(EnergyType.Willpower)}
  set will(val) {this.set(EnergyType.Willpower, val)}
  get health() {return this.get(EnergyType.Health)}
  set health(val) {this.set(EnergyType.Health, val)}
  get fire() {return this.get(EnergyType.Fire)}
  set fire(val) {this.set(EnergyType.Fire, val)}
  get ice() {return this.get(EnergyType.Ice)}
  set ice(val) {this.set(EnergyType.Ice, val)}
  get elec() {return this.get(EnergyType.Electricity)}
  set elec(val) {this.set(EnergyType.Electricity, val)}
  get light() {return this.get(EnergyType.Light)}
  set light(val) {this.set(EnergyType.Light, val)}
  get dark() {return this.get(EnergyType.Dark)}
  set dark(val) {this.set(EnergyType.Dark, val)}

  constructor(energies: Energies | EnergiesI) {
    if (energies instanceof Energies) {
      super(energies)
    } else {
      super(0)
      for (const type in EnergyType) {
        const energyType = EnergyType[type] as EnergyType
        const value = energies[energyType]
        if (value !== undefined) this.set(energyType, value)
      }
    }
  }

  // Create a partial Energies struct from MonStats
  static fromStats(stats: MonStats): Energies {
    return new Energies({
      stam: stats.con,
      will: stats.wis,
      health: stats.frt,
    })
  }

  static fromI(src: EnergiesI): Energies {
    return new Energies(src)
  }

  add(other: Energies | EnergiesI): Energies {
    return new Energies(other).mergeWith(this, (l, r) => r + l)
  }

  sub(other: EnergiesI): Energies {
    return new Energies(other).mergeWith(this, (l, r) => r - l)
  }

  addAssign(other: Energies | EnergiesI): Energies {
    let energies: Energies
    if (!(other instanceof Energies)) energies = new Energies(other)
    else energies = other
    return this.mergeWith(energies, (l, r) => l + r)
  }
  subAssign(other: Energies | EnergiesI): Energies {
    let energies: Energies
    if (!(other instanceof Energies)) energies = new Energies(other)
    else energies = other
    return this.mergeWith(energies, (l, r) => l - r)
  }

  toString(): string {
    return [...this.entries()].map(([type, val]) => `${type}: ${val}`).join('\n')
  }

  anyNegative(): boolean {
    for (const val of this.values()) {
      if (val < 0) return true
    }
    return false
  }
}
