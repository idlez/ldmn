import { Aspect } from '../aspect'
import { Monster } from '../monster';
import { makeListener, GainedEnergyListener, GainedEnergyEvent } from '../event';
import { Uuid } from '../util/uuid';

const EXHAUSTED_UUID = new Uuid()
export class Exhausted implements Aspect<Monster> {
  uuid = EXHAUSTED_UUID
  name = "Exhausted"
  desc = "This guy is all tuckered out."
  enabled = true
  important = true
  hidden = false

  onEnabled(subject: Monster) {
    if (subject.energy().stam <= 0) subject.addIncapacitatingFactor(this)
  }

  onDisabled(subject: Monster) {
    subject.removeIncapactiatingFactor(this.uuid)
  }

  listeners = [
    makeListener<Monster, GainedEnergyListener<Monster>>("GainedEnergy",
      (ev: GainedEnergyEvent<Monster>) => {
        console.log("[Exhausted] onGainedEnergy aspect behavior run")
        // Remove self when stam rises above zero
        if (ev.source.energy().stam > 0) {
          ev.source.removeAspect(this.uuid)
        }
      }
    ),
  ]
}