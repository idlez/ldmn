import { Monster, StatBoostMultiplicative, makeStatBoost } from "../monster";
import { Aspect } from "../aspect";
import { Uuid } from "../util/uuid";

const BLOCKING_STAT_BOOST: StatBoostMultiplicative = makeStatBoost({
  frt: 1.1
})

const BLOCKING_UUID = new Uuid()

export class Blocking implements Aspect<Monster> {
  uuid = BLOCKING_UUID
  name = "Blocking"
  desc = "Defenses raised against incoming blows."
  enabled = true
  important = true
  hidden = false

  boostId?: Uuid

  constructor() {}

  onEnabled(sub: Monster) {
    if (!this.boostId) {
      this.boostId = new Uuid()
      sub.addStatBoost(this.boostId, BLOCKING_STAT_BOOST)
    }
  }

  onDisabled(sub: Monster) {
    if (this.boostId) sub.removeStatBoost(this.boostId)
    this.boostId = undefined
  }
}