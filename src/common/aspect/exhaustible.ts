import { Aspect } from '../aspect';
import { Monster } from '../monster';
import { makeListener, LostEnergyEvent, LostEnergyListener } from '../event';
import { Exhausted } from './exhausted';
import { Uuid } from '../util/uuid';

const EXHAUSTIBLE_UUID = new Uuid()
export class Exhaustible implements Aspect<Monster> {
  uuid = EXHAUSTIBLE_UUID
  name = "Exhaustible"
  desc = "This Monster will become exhausted when its stamina reaches 0."
  enabled = true
  important = false
  hidden = false

  onEnabled(subject: Monster) {
    if (subject.energy().stam <= 0) {
      // Nighty-night
      subject.addAspect(new Exhausted())
    }
  }

  listeners = [
    makeListener<Monster, LostEnergyListener<Monster>>("LostEnergy",
      (ev: LostEnergyEvent<Monster>) => {
        if (ev.source.energy().stam <= 0) {
          // Nighty-night
          ev.source.addAspect(new Exhausted())
        }
      }
    ),
  ]
}