import { Aspect } from '../aspect'
import { Monster } from '../monster';
import { Uuid } from '../util/uuid';

const DEAD_UUID = new Uuid()
export class Dead implements Aspect<Monster> {
  uuid = DEAD_UUID
  name = "Dead"
  desc = "The opposite of alive is what this is"
  enabled = true
  important = true
  hidden = false

  onEnabled(subject: Monster) {
    subject.addIncapacitatingFactor(this)
  }

  onDisabled(subject: Monster) {
    subject.removeIncapactiatingFactor(this.uuid)
  }
}