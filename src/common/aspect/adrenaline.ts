import {Aspect} from '../aspect'
import { Monster, StatBoostAdditive, makeStatBoost } from '../monster'
import { TookDamageListener, TookDamageEvent, makeListener } from '../event'
import { Uuid } from '../util/uuid';

const ADRENALINE_STAT_BOOST: StatBoostAdditive = makeStatBoost({
  dex: 20, str: 20, spd: 20
}, 'before')

const ADRENALINE_UUID = new Uuid()

export class Adrenaline implements Aspect<Monster> {
  uuid = ADRENALINE_UUID
  name = "Adrenaline"
  desc = "Gain a boost of energy in a pinch."
  enabled = false
  important = true
  hidden = false

  boostId?: Uuid

  onAdded = (subject: Monster) => {
    const maxHealth = subject.maxEnergy().health
    const curHealth = subject.energy().health
    const criticalHealth = maxHealth * 0.2
    if (curHealth <= criticalHealth) {
      subject.enableAspect("Adrenaline")
    }
  }

  onEnabled = (subject: Monster) => {
    if (!this.boostId) {
      this.boostId = new Uuid()
      subject.addStatBoost(this.boostId, ADRENALINE_STAT_BOOST)
    }
  }

  onDisabled = (subject: Monster) => {
    if (this.boostId) subject.removeStatBoost(this.boostId)
    this.boostId = undefined
  }

  listeners = [
    makeListener<Monster, TookDamageListener<Monster>>("TookDamage",
      (ev: TookDamageEvent<Monster>) => {
        const maxHealth = ev.source.maxEnergy().health
        const curHealth = ev.source.energy().health
        const criticalHealth = maxHealth * 0.2
    
        // One more hit like that and you're a gonner
        // Note that Adrenaline will not trigger as easily when taking many
        // instances of small damage.
        if (curHealth <= criticalHealth || curHealth <= ev.damage) {
          ev.source.enableAspect(ADRENALINE_UUID)
        }
      }
    ),
  ]
}
