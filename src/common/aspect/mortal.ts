import { Aspect } from '../aspect';
import { Monster } from '../monster';
import { makeListener, LostEnergyEvent, LostEnergyListener } from '../event';
import { Dead } from './dead';
import { Uuid } from '../util/uuid';

const MORTAL_UUID = new Uuid()
export class Mortal implements Aspect<Monster> {
  uuid = MORTAL_UUID
  name = "Mortal"
  desc = "This Monster will die when its health reaches 0."
  enabled = true
  important = false
  hidden = false

  onEnabled(subject: Monster) {
    if (subject.energy().health <= 0) {
      // F
      subject.addAspect(new Dead())
    }
  }

  listeners = [
    makeListener<Monster, LostEnergyListener<Monster>>("LostEnergy",
      (ev: LostEnergyEvent<Monster>) => {
        if (ev.source.energy().health <= 0) {
          // F
          ev.source.addAspect(new Dead())
        }
      }
    ),
  ]
}