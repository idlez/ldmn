import { MapDefault } from './util/map_default';
export enum DamageType {
  Force = "force",
  Slash = "slash",
  Blunt = "blunt",
  Pierce = "pierce",
  Crush = "crush",
  Heat = "heat",
  Cold = "cold",
  Zap = "zap",
  Light = "light",
  Dark = "dark",
  Mental = "mental",
  Acid = "acid",
  Toxic = "toxic",
  Decay = "decay",
  Vibration = "vibration",
  Exertion = "exertion"
}
export type DamageSpreadI = {
  [T in DamageType]?: number;
};
export class DamageSpread extends MapDefault<DamageType, number> {
  constructor(spread: DamageSpread | DamageSpreadI) {
    // Clone path
    if (spread instanceof DamageSpread) {
      super(spread);
    }
    // Create path
    else {
      super(0);
      for (const type in DamageType) {
        const damageType = DamageType[type] as DamageType;
        const value = spread[damageType];
        if (value !== undefined)
          this.set(damageType, value);
      }
    }
  }

  toString(): string {
    return [...this.entries()].map(([type, val]) => `${type}: ${val}`).join('\n')
  }
}
