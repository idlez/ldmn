import { Aspect } from './aspect';
import { EnergiesI } from './monster';
import { DamageType, DamageSpreadI } from "./damage";
import { Omit } from './util/types';

/* --- Define game-specific events and related types --- */

/* Algebraic Data Types for ease of consumption */
export type LDEvent<S> =
  | AddingAspectEvent<S> | AddedAspectEvent<S>
  | RemovingAspectEvent<S> | RemovedAspectEvent<S>
  | EnablingAspectEvent<S> | EnabledAspectEvent<S>
  | DisablingAspectEvent<S> | DisabledAspectEvent<S>
  | TimePassedEvent<S>
  | TakingDamageEvent<S> | TookDamageEvent<S>
  | LosingEnergyEvent<S> | LostEnergyEvent<S>
  | GainingEnergyEvent<S> | GainedEnergyEvent<S>
;
export type LDCancellableEvent<S> =
  | AddingAspectEvent<S>
  | RemovingAspectEvent<S>
  | EnablingAspectEvent<S>
  | DisablingAspectEvent<S>
  | TakingDamageEvent<S>
  | LosingEnergyEvent<S>
  | GainingEnergyEvent<S>
;
export type LDListener<S> =
  | AddingAspectListener<S> | AddedAspectListener<S>
  | RemovingAspectListener<S> | RemovedAspectListener<S>
  | EnablingAspectListener<S> | EnabledAspectListener<S>
  | DisablingAspectListener<S> | DisabledAspectListener<S>
  | TimePassedListener<S>
  | TakingDamageListener<S> | TookDamageListener<S>
  | LosingEnergyListener<S> | LostEnergyListener<S>
  | GainingEnergyListener<S> | GainedEnergyListener<S>
;
export type LDCancellableListener<S> =
  | AddingAspectListener<S>
  | RemovingAspectListener<S>
  | EnablingAspectListener<S>
  | DisablingAspectListener<S>
  | TakingDamageListener<S>
  | LosingEnergyListener<S>
  | GainingEnergyListener<S>
;

/* Emitter definition; mostly a tag */
export interface LDEventEmitter<S> {
  addEventListener(listener: LDListener<S>): void
  removeEventListener(listener: LDListener<S>): /* didRemove: */ boolean
}

/* Convenience functions */
export function makeListener<S, L extends LDListener<S>> (
  listenerType: L["listenerType"],
  listenerBehavior: Omit<L, "listenerType">,
): L
{
  return Object.assign(listenerBehavior, {
    listenerType,
  }) as L // I sure hope the type checker is dumber than me here
}


/* --- Define Events and their respective Listeners --- */

// For extending and simplifying the definition of items below
interface LDEventProto<S> {
  source: S
}
interface LDCancellableEventProto<S> extends LDEventProto<S> {}

interface LDEventListenerProto<S, E extends LDEvent<S>, R> {
  (event: E): R
}

export type ShouldCancel = boolean
type LDCancellableEventListenerProto<S, E extends LDCancellableEvent<S>> =
  LDEventListenerProto<S, E, ShouldCancel>
;

// Emitted before another Aspect is added to the source
export interface AddingAspectEvent<S> extends LDCancellableEventProto<S> {
  eventType: "AddingAspect"
  aspect: Aspect<S>
}
export interface AddingAspectListener<S> extends LDCancellableEventListenerProto<S, AddingAspectEvent<S>> {
  listenerType: "AddingAspect"
}

// Emitted after another Aspect is added to the source
export interface AddedAspectEvent<S> extends LDEventProto<S> {
  eventType: "AddedAspect"
  aspect: Aspect<S>
}
export interface AddedAspectListener<S> extends LDEventListenerProto<S, AddedAspectEvent<S>, void> {
  listenerType: "AddedAspect"
}

// Emitted before another Aspect is removed from the source
export interface RemovingAspectEvent<S> extends LDCancellableEventProto<S> {
  eventType: "RemovingAspect"
  aspect: Aspect<S>
}
export interface RemovingAspectListener<S> extends LDCancellableEventListenerProto<S, RemovingAspectEvent<S>> {
  listenerType: "RemovingAspect"
}

// Emitted after another Aspect is removed from the source
export interface RemovedAspectEvent<S> extends LDEventProto<S> {
  eventType: "RemovedAspect"
  aspect: Aspect<S>
}
export interface RemovedAspectListener<S> extends LDEventListenerProto<S, RemovedAspectEvent<S>, void> {
  listenerType: "RemovedAspect"
}

// Emitted before another Aspect is enabled
export interface EnablingAspectEvent<S> extends LDCancellableEventProto<S> {
  eventType: "EnablingAspect"
  aspect: Aspect<S>
}
export interface EnablingAspectListener<S> extends LDCancellableEventListenerProto<S, EnablingAspectEvent<S>> {
  listenerType: "EnablingAspect"
}

// Emitted after another Aspect is enabled
export interface EnabledAspectEvent<S> extends LDEventProto<S> {
  eventType: "EnabledAspect"
  aspect: Aspect<S>
}
export interface EnabledAspectListener<S> extends LDEventListenerProto<S, EnabledAspectEvent<S>, void> {
  listenerType: "EnabledAspect"
}

// Emitted before another Aspect is disabled
export interface DisablingAspectEvent<S> extends LDCancellableEventProto<S> {
  eventType: "DisablingAspect"
  aspect: Aspect<S>
}
export interface DisablingAspectListener<S> extends LDCancellableEventListenerProto<S, DisablingAspectEvent<S>> {
  listenerType: "DisablingAspect"
}

// Emitted after another Aspect is disabled
export interface DisabledAspectEvent<S> extends LDEventProto<S> {
  eventType: "DisabledAspect"
  aspect: Aspect<S>
}
export interface DisabledAspectListener<S> extends LDEventListenerProto<S, DisabledAspectEvent<S>, void> {
  listenerType: "DisabledAspect"
}

// Emitted after some IN-GAME time has passed
export interface TimePassedEvent<S> extends LDEventProto<S> {
  eventType: "TimePassed"
  delta: number // Unit TBD
}
export interface TimePassedListener<S> extends LDEventListenerProto<S, TimePassedEvent<S>, void> {
  listenerType: "TimePassed"
}

// Emitted before taking damage
export interface TakingDamageEvent<S> extends LDEventProto<S> {
  eventType: "TakingDamage"
  damage: DamageSpreadI
}
export interface TakingDamageListener<S>
  extends LDEventListenerProto<S, TakingDamageEvent<S>, /* altered damage spread */ DamageSpreadI>
{
  listenerType: "TakingDamage"
}

// Emitted after taking damage
export interface TookDamageEvent<S> extends LDEventProto<S> {
  eventType: "TookDamage"
  damage: DamageSpreadI
}
export interface TookDamageListener<S> extends LDEventListenerProto<S, TookDamageEvent<S>, void> {
  listenerType: "TookDamage"
}

export type EnergyLossCause = DamageType

// Emitted before losing energy
export interface LosingEnergyEvent<S> extends LDEventProto<S> {
  eventType: "LosingEnergy"
  energies: EnergiesI
  cause: EnergyLossCause
}
export interface LosingEnergyListener<S>
  extends LDEventListenerProto<S, LosingEnergyEvent<S>, /* altered energies to lose */ EnergiesI>
{
  listenerType: "LosingEnergy"
}

// Emitted after losing energy
export interface LostEnergyEvent<S> extends LDEventProto<S> {
  eventType: "LostEnergy"
  energies: EnergiesI
  cause: EnergyLossCause
}
export interface LostEnergyListener<S> extends LDEventListenerProto<S, LostEnergyEvent<S>, void> {
  listenerType: "LostEnergy"
}

export type EnergyGainCause = 'Time' | 'Heal'

// Emitted before gaining energy
export interface GainingEnergyEvent<S> extends LDEventProto<S> {
  eventType: "GainingEnergy"
  energies: EnergiesI
  cause: EnergyGainCause
}
export interface GainingEnergyListener<S>
  extends LDEventListenerProto<S, GainingEnergyEvent<S>, /* altered energies to lose */ EnergiesI>
{
  listenerType: "GainingEnergy"
}

// Emitted after gaining energy
export interface GainedEnergyEvent<S> extends LDEventProto<S> {
  eventType: "GainedEnergy"
  energies: EnergiesI
  cause: EnergyGainCause
}
export interface GainedEnergyListener<S> extends LDEventListenerProto<S, GainedEnergyEvent<S>, void> {
  listenerType: "GainedEnergy"
}