import { Monster } from "./monster";
import { Battle } from "./battle";

export interface Skill {
  name: string
  desc: string

  actionsFor(actor: Monster, context: Battle): Action<Monster, any, Battle>[]
}

import { Aspect } from './aspect'
import { Uuid } from './util/uuid';
import { assertNever } from "./util/types";

// A Actor: The thing performing this action
// T Target: The thing(s) affected by this action
// C Context: The context of this action (like Battle)
export interface Action<A, T, C> {
  perform: (context: C, tickOffset: number, totalTicks: number) => void, // mutates A, T, C
  uuid: Uuid
  name: string
  desc: string
  actor: A
  target: T
  aspects: Aspect<Action<A, T, C>>[]
  duration: ActionDuration
}

export type SpeedModProfile = 'noMain' | 'all'

export class ActionDuration {
  pre: number
  main: number
  post: number

  constructor(duration: number)
  constructor(other: ActionDuration)
  constructor(pre: number, main: number, post: number)
  constructor(preOrMainOrOther: number | ActionDuration, main?: number, post?: number) {
    if (preOrMainOrOther instanceof ActionDuration) {
      this.pre = preOrMainOrOther.pre
      this.main = preOrMainOrOther.main
      this.post = preOrMainOrOther.post
    }
    else if (main === undefined) {
      this.pre = 0
      this.main = preOrMainOrOther
      this.post = 0
    }
    else {
      this.pre = preOrMainOrOther
      this.main = main
      this.post = post || 0
    }
  }

  withSpeedMod(profile: SpeedModProfile, speed: number): this {
    const speedMod = 100 / speed
    switch (profile) {
      case 'noMain': {
        this.pre = Math.ceil(this.pre * speedMod)
        this.post = Math.ceil(this.post * speedMod)
      } break
      case 'all': {
        this.pre = Math.ceil(this.pre * speedMod)
        this.main = Math.ceil(this.main * speedMod)
        this.post = Math.ceil(this.post * speedMod)
      } break
      default: assertNever(profile)
    }

    return this
  }

  cloneWithSpeedMod(profile: SpeedModProfile, speed: number): ActionDuration {
    return new ActionDuration(this).withSpeedMod(profile, speed)
  }
}
