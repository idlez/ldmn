import { Color } from './color';

/**
 * Generate an integer in range [min, max).
 * @param min defaults to 0
 * @param max defaults to Number.MAX_SAFE_INTEGER
 */
export function randInt(min?: number, max?: number): number {
  min = min ? Math.ceil(min) : 0
  max = max ? Math.floor(max) : Number.MAX_SAFE_INTEGER
  return Math.floor(Math.random() * (max - min + 1)) + min
}

/**
 * Standard Normal variate using Box-Muller transform
 * 
 * Adapted from https://stackoverflow.com/a/36481059
 * NOTE: On 1000 samples, the range is [-3.10, 3.24] with center=0, width=1
 */
interface RandNormalParams {
  center?: number, width?: number, clampMin?: number, clampMax?: number
}
export function randNormal(params?: RandNormalParams): number {
  let u = 0, v = 0
  // Converting Math.random()'s [0,1) to (0,1). Should rarely loop.
  while(u === 0) u = Math.random()
  while(v === 0) v = Math.random()
  let ret = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v )

  // Transforms for convenience
  if (params !== undefined) {
    const {center, width, clampMin, clampMax} = params
    if (width) ret *= width
    if (center) ret += center
    if (clampMin && ret < clampMin) return clampMin
    if (clampMax && ret > clampMax) return clampMax
  }
  return ret
}
// And a higher-order version for easily passing around normal dist generators
export function makeRandNormal(params: RandNormalParams): () => number {
  return () => randNormal(params)
}