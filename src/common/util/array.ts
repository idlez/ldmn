export function range(start: number, stop: number, step = 1): ReadonlyArray<number> {
  return Array(Math.ceil((stop - start) / step)).fill(start).map((x, y) => x + y * step)
}