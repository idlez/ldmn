
/**
 * Type-checking helper for a branch that shouldn't ever execute
 */
export function assertNever(x: never): never {
  throw new Error(`Unexpected value: ${x}`)
}

export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>