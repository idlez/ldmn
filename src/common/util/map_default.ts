/**
 * A Map that falls back to a provided default value.
 */
export class MapDefault<K, V> implements Map<K, V> {

  public defaultVal: V
  private innerMap: Map<K, V>

  constructor(defaultVal: V)
  constructor(toClone: MapDefault<K, V>)
  constructor(defaultVal: V, entries: ReadonlyArray<[K, V]> | Iterable<[K, V]>)
  constructor(defaultVal: V | MapDefault<K, V>, entries?: ReadonlyArray<[K, V]> | Iterable<[K, V]>) {
    // Clone path
    if (defaultVal instanceof MapDefault) {
      this.innerMap = new Map<K, V>(defaultVal.entries())
      this.defaultVal = defaultVal.defaultVal
    }
    // Create path
    else {
      this.innerMap = new Map<K, V>(entries as any) // it's fine, Map can take it
      this.defaultVal = defaultVal
    }
  }

  get(key: K, customDefault?: V): V {
    const ret = this.innerMap.get(key)
    return ret === undefined 
      ? customDefault === undefined
        ? this.defaultVal
        : customDefault
      : ret
  }

  getWithoutDefault(key: K): V | undefined {
    return this.innerMap.get(key)
  }

  // Merge another Map into this one.
  // If no merge function is provided, replace values in this map with the ones in the other.
  mergeWith(other: Map<K, V>, mergeFunc?: (curVal: V, otherVal: V) => V): this {
    for (let [key, val] of other.entries()) {
      if (mergeFunc) val = mergeFunc(this.get(key), val)
      this.set(key, val)
    }
    return this
  }

  static fromMerge<K, V>
    (defaultVal: V, lMap: Map<K, V>, rMap: Map<K, V>, mergeFunc?: (lVal: V, rVal: V) => V): MapDefault<K, V>
  {
    return new MapDefault<K, V>(defaultVal, lMap).mergeWith(rMap, mergeFunc)
  }
  
  // Pass-through
  clear() {this.innerMap.clear()}
  set(key: K, value: V) {this.innerMap.set(key, value); return this}
  delete(key: K) {return this.innerMap.delete(key)}
  has(key: K) {return this.innerMap.has(key)}
  forEach(callbackfn: (value: V, key: K, map: Map<K, V>) => void, thisArgs?: any) {return this.innerMap.forEach(callbackfn, thisArgs)}
  get size() {return this.innerMap.size}
  entries() {return this.innerMap.entries()}
  keys() {return this.innerMap.keys()}
  values() {return this.innerMap.values()}

  [Symbol.iterator]() {
    return this.innerMap.entries()
  }

  get [Symbol.toStringTag]() {return this.innerMap[Symbol.toStringTag]}
}
