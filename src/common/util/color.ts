import { stringHashCode } from './string';

// naive color class
export class Color {
  constructor(
    public r: number,
    public g: number,
    public b: number,
    public a: number
  ) {}

  static fromStringHash(str: string): Color {
    const hash = stringHashCode(str)
    const r = (hash & 0xFF0000) >> 16
    const g = (hash & 0x00FF00) >> 8
    const b = (hash & 0x0000FF) >> 0
    return new Color(r, g, b, 255)
  }

  // ignores alpha
  toString(): string {
    const rs = this.r.toString(16).padStart(2, '0')
    const gs = this.g.toString(16).padStart(2, '0')
    const bs = this.b.toString(16).padStart(2, '0')
    return `#${rs}${gs}${bs}`
  }

  // this is how color spaces work, right?
  brighten(by: number): this {
    this.r += by
    this.g += by
    this.b += by
    return this
  }

  darken(by: number): this {
    this.r -= by
    this.g -= by
    this.b -= by
    return this
  }
}