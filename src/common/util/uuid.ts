import { Color } from './color';

/**
 * A 128-bit UUID
 */
export class Uuid {

  backingArray: Uint32Array // of length 4 for 128 bits
  stringRepr?: string

  constructor() {
    this.backingArray = new Uint32Array(4)
    crypto.getRandomValues(this.backingArray)
    return this
  }

  toString(): string {
    if (this.stringRepr === undefined) {
      this.stringRepr = this.backingArray.reduce(
        (str, num) => str + num.toString(16).padStart(16, '0'),
        "",
      )
    }
    return this.stringRepr
  }

  toColor(): Color {
    const u32 = this.backingArray[0]
    const r = (u32 >> 24) & 0xFF
    const g = (u32 >> 16) & 0xFF
    const b = (u32 >>  8) & 0xFF
    return new Color(r, g, b, 0xFF)
  }

  equals(other: Uuid): boolean {
    return this.backingArray[0] === other.backingArray[0]
      && this.backingArray[1] === other.backingArray[1]
      && this.backingArray[2] === other.backingArray[2]
      && this.backingArray[3] === other.backingArray[3]
  }
}