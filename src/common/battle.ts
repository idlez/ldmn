import {Monster} from './monster'
import { assertNever } from './util/types';
import { Action } from './skill';

interface WindupTick<A> {
  tickType: "Windup"
  offset: number
  totalTicks: number
  forAction: Action<A, any, Battle>
}
interface ActiveTick<A> {
  tickType: "Active"
  offset: number
  totalTicks: number
  forAction: Action<A, any, Battle>
}
interface CooldownTick<A> {
  tickType: "Cooldown"
  offset: number
  totalTicks: number
  forAction: Action<A, any, Battle>
}

export type BTick<A> = WindupTick<A> | ActiveTick<A> | CooldownTick<A>

export class  BTrack<A extends Monster> {

  ticks: (BTick<A> | undefined)[]
  curTime: number
  availableActions: Action<A, any, Battle>[] | null

  constructor(public actor: A, public battle: Battle) {
    this.ticks = []
    this.availableActions = null
    this.curTime = 0
  }

  needsAction(): boolean {
    return this.ticks[this.curTime] === undefined
  }

  get curTick(): BTick<A> | undefined {
    return this.ticks[this.curTime]
  }

  tick() {
    const t = this.curTick
    if (t !== undefined) {
      this.availableActions = null
      switch (t.tickType) {
        case 'Windup':
        case 'Cooldown':
          break
        case 'Active':
          t.forAction.perform(this.battle, t.offset, t.totalTicks)
          break
        default: assertNever(t)
      }
    } else {
      this.availableActions = this.actor.actionsIn(this.battle)
    }
    this.curTime++
  }

  getActions(): Action<A, any, Battle>[] {
    if (this.availableActions === null) {
      if (this.curTick === undefined) {
        this.availableActions = this.actor.actionsIn(this.battle)
        return this.availableActions
      }
      else {
        return []
      }
    }
    else {
      return this.availableActions
    }
  }

  scheduleAction(act: Action<A, any, Battle>, when?: {in: number} | {at: number}) {
    let time: number
    if (when === undefined) time = this.curTime
    else {
      if ('in' in when) {
        time = this.curTime + when.in
      } else {
        time = when.at
      }
    }

    const {pre, main, post} = act.duration

    for (let offset = 0; offset < pre; offset++) {
      this.ticks[time + offset] = {
        tickType: 'Windup',
        offset,
        totalTicks: pre,
        forAction: act,
      }
    }
    for (let offset = 0; offset < main; offset++) {
      this.ticks[time + pre + offset] = {
        tickType: 'Active',
        offset,
        totalTicks: main,
        forAction: act,
      }
    }
    for (let offset = 0; offset < post; offset++) {
      this.ticks[time + pre + main + offset] = {
        tickType: 'Cooldown',
        offset,
        totalTicks: post,
        forAction: act,
      }
    }

  }

}
export class BTimeline {
  tracks: BTrack<Monster>[]

  constructor(participants: Monster[], context: Battle) {
    this.tracks = participants.map(p => new BTrack(p, context))

  }

  tickAll(): true | number[] {
    if (this.tracks.every(track => !track.needsAction())) {
      for (const track of this.tracks) {
        track.tick()
      }
      return true
    } else {
      return this.tracks.reduce((indices, cur, index) => {
        if (cur.needsAction()) indices.push(index)
        return indices 
      }, new Array<number>())
    }
  }

  trackFor(mon: Monster): BTrack<Monster> | undefined {
    return this.tracks.find(track => track.actor.getId() === mon.getId())
  }

}

export class Battle {
  timeline: BTimeline
  log: string[]
  participants: Monster[]
    
  constructor(public lmon: Monster, public rmon: Monster) {
    this.participants = [lmon, rmon]
    this.timeline = new BTimeline(this.participants, this)
    this.log = ["Battle Started"]
  }

  logAsMon(mon: Monster, line: string) {
    this.log.push(`${mon.displayName()}: ${line}`)
  }

  getOpposingMons(from: Monster) {
    return this.participants.filter(mon => mon.getId() !== from.getId())
  }

  processUntilNeedsAction(): [Monster, Action<Monster, any, Battle>[]][] {
    let actionIndices: true | number[]
    do {
      actionIndices = this.timeline.tickAll()
    } while (actionIndices === true)
    
    return actionIndices.map(index => {
      const track = this.timeline.tracks[index]
      let ret: [Monster, Action<Monster, any, Battle>[]]
      ret = [track.actor, track.getActions()]
      return ret
    })

  }

  submitAction(act: Action<Monster, any, Battle>): boolean {
    const track = this.timeline.trackFor(act.actor)
    if (track === undefined) return false
    track.scheduleAction(act)
    return true
  }

  actionsFor(index: number): Action<any, any, Battle>[] {
    return this.timeline.tracks[index].actor.actionsIn<Battle>(this)
  }

}
