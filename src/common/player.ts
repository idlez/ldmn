import { Monster } from "./monster";
import { Uuid } from "./util/uuid";

export class Player {
  uuid: Uuid
  name: string
  party: Monster[]

  constructor() {
    this.uuid = new Uuid()
    this.name = "TestPlayer1"
    this.party = []
  }
}