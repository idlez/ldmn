import { Battle } from "../battle";
import { Monster } from "../monster";
import { Blocking } from "../aspect/blocking";
import { Uuid } from '../util/uuid';
import { Skill, Action, ActionDuration } from "../skill";

export class BlockSkill implements Skill {
  name = "Block"
  desc = "Raise defences against incoming physical attacks."

  actionsFor = (actor: Monster, context: Battle) => {
    return [new BlockAction(actor)]
  }
}

export class BlockAction implements Action<Monster, void, Battle> {
  uuid = new Uuid()
  name = "Block"
  desc = "Raise defences against incoming physical attacks."
  target = void 0
  aspects = []
  duration = new ActionDuration(1, 1, 0).withSpeedMod('noMain', this.actor.calcEffectiveStats().spd)

  constructor(public actor: Monster) {}

  perform = (bat: Battle) => {
    this.actor.addAspect(new Blocking())
    bat.logAsMon(this.actor, "Prepared for incoming attacks.")
  }
}