import { ActionDuration } from "../skill";
import { Energies } from "../monster";
import { DamageSpread } from "../damage";
import { AttackSkill } from "./attack";

export const StrikeSkill = new AttackSkill({
  name: "Strike",
  desc: "Hit the target with an appendage.",
  targetType: 'single',
  cost: Energies.fromI({ stam: 10 }),
  powers: new DamageSpread({ blunt: 10 }),
  duration: new ActionDuration(5, 1, 1),
})
