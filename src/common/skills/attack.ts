import { DamageSpread, DamageType, DamageSpreadI } from '../damage';
import { Battle } from '../battle';
import { Monster, Energies } from '../monster';
import { Aspect } from '../aspect';
import { makeRandNormal } from '../util/rand';
import { Uuid } from '../util/uuid';
import { Skill, Action, ActionDuration, SpeedModProfile } from '../skill';
import { LUCK_MOD, DAMAGE_MOD } from '@common/constant';

const DEFAULT_NORMAL_CURVE = makeRandNormal({center: 1, width: 0.1, clampMin: 0.7, clampMax: 1.3})

export type DamageCurve = (rand: number) => /* multiplier */ number
export type CurvesForDamageTypes = {
  [T in DamageType]: DamageCurve
}
export type TargetType = 'single'

export interface AttackInitializer {
  targetType: TargetType
  name: string
  desc: string
  duration: ActionDuration
  speedMod?: SpeedModProfile
  cost: Energies
  powers: DamageSpread
  varianceCurve?: DamageCurve | CurvesForDamageTypes
}
export class AttackSkill implements Skill {
  uuid: Uuid
  targetType: TargetType
  name: string
  desc: string
  duration: ActionDuration
  speedMod: SpeedModProfile
  aspects: Aspect<Action<Monster, any, Battle>>[] = []

  cost: Energies
  powers: DamageSpread
  varianceCurve: DamageCurve | CurvesForDamageTypes
  singleCurve: boolean

  isDamageCurve(curve: DamageCurve | CurvesForDamageTypes): curve is DamageCurve {
    return this.singleCurve
  }

  constructor(init: AttackInitializer) {
    this.uuid = new Uuid()
    const { targetType, name, desc, cost, powers, duration, speedMod } = init
    this.targetType = targetType
    this.name = name
    this.desc = desc
    this.cost = cost
    this.powers = powers
    this.duration = duration
    this.speedMod = speedMod || 'noMain'
    if (init.varianceCurve !== undefined) {
      this.varianceCurve = init.varianceCurve
    } else {
      this.varianceCurve = DEFAULT_NORMAL_CURVE
    }
    // Handle type checking here
    this.singleCurve = this.varianceCurve instanceof Function

  }
  
  actionsFor(actor: Monster, context: Battle): AttackAction<Monster>[] {
    
    if (actor.predictRemainingEnergy(this.cost, DamageType.Exertion).anyNegative()) {
      return [] // not enough energy
    }

    // Handle speed affecting duration
    const speed = actor.calcEffectiveStats().spd
    const duration = this.duration.cloneWithSpeedMod(this.speedMod, speed)
    return context.getOpposingMons(actor).map(target =>
      new AttackAction({
        name: this.name,
        desc: this.desc, // TODO: Add target, duration, cost summary
        actor,
        target,
        duration,
        cost: this.cost,
        powers: this.powers,
        varianceCurve: this.varianceCurve,
        singleCurve: this.singleCurve,
      })
    )

  }
}

interface AttackActionInitializer<T extends Monster | Monster[]> {
  actor: Monster
  target: T
  name: string
  desc: string
  duration: ActionDuration
  cost: Energies
  powers: DamageSpread
  varianceCurve: DamageCurve | CurvesForDamageTypes
  singleCurve: boolean
}
export class AttackAction<T extends Monster | Monster[]> implements Action<Monster, T, Battle> {
  uuid: Uuid
  actor: Monster
  target: T
  name: string
  desc: string
  duration: ActionDuration
  aspects: Aspect<Action<Monster, T, Battle>>[] = []

  cost: Energies
  powers: DamageSpread
  varianceCurve: DamageCurve | CurvesForDamageTypes
  singleCurve: boolean

  isDamageCurve(curve: DamageCurve | CurvesForDamageTypes): curve is DamageCurve {
    return this.singleCurve
  }

  constructor(init: AttackActionInitializer<T>) {
    this.uuid = new Uuid()
    const {actor, target, name, desc, cost, powers, duration} = init
    this.actor = actor
    this.target = target
    this.name = name
    this.desc = desc
    this.cost = cost
    this.powers = powers
    this.duration = duration
    if (init.varianceCurve !== undefined) {
      this.varianceCurve = init.varianceCurve
    } else {
      this.varianceCurve = DEFAULT_NORMAL_CURVE
    }
    // Handle type checking here
    this.singleCurve = this.varianceCurve instanceof Function
  }

  perform = (bat: Battle, offset: number) => {
    
    const remainingEnergy = this.actor.predictRemainingEnergy(this.cost, DamageType.Exertion)
    const hasEnergy = !remainingEnergy.anyNegative()
    
    if (!hasEnergy) {
      bat.logAsMon(this.actor, `Couldn't ${this.name}; not enough energy`)
      return // early return on failure
    }

    this.actor.drainEnergy(this.cost, DamageType.Exertion)

    for (const tar of this.target instanceof Monster ? [this.target] : this.target) {
      
      const ast = this.actor.calcEffectiveStats()
      const tst = tar.calcEffectiveStats()

      const luckRatioScaled = Math.pow(ast.luk / tst.luk, LUCK_MOD)

      // Random value [0, 1) weighted to favor the one with greater luck
      const rand = Math.pow(Math.random(), tst.luk / ast.luk)
      // This attack's random multiplier derived from the value above and the supplied curve
      const R = this.isDamageCurve(this.varianceCurve) ? this.varianceCurve(rand) : undefined
  
      const damages: DamageSpreadI = {}
      for (const [type, power] of this.powers.entries()) {
        // Use this damage type's curve if this attack uses individual curves.
        // Otherwise use the already-computed R value.
        const Rl = R || (this.varianceCurve as CurvesForDamageTypes)[type](rand)

        // Calculate damage to deal
        const power = this.powers.get(type)
        const statRatio = this.actor.offensiveStatFor(type) / tar.defensiveStatFor(type)
        let damage = Math.floor(power * statRatio * luckRatioScaled * Rl * DAMAGE_MOD)

        // And add that to damages
        damages[type] = damage
      }

      tar.takeDamage(damages)
  
      bat.logAsMon(this.actor, `(${this.name}) Attacked ${tar.displayName()}`)
      bat.logAsMon(tar, `Took ${JSON.stringify(damages)} damage`)
    }

  }
}
