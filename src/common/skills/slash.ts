import { ActionDuration } from "../skill";
import { Energies } from "../monster";
import { DamageSpread } from "../damage";
import { AttackSkill } from "./attack";

export const SlashSkill = new AttackSkill({
  name: "Slash",
  desc: "Slash the target with an appendage.",
  targetType: 'single',
  cost: Energies.fromI({ stam: 10 }),
  powers: new DamageSpread({ slash: 10 }),
  duration: new ActionDuration(5, 1, 1),
})
