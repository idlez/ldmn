import { Battle } from "../battle";
import { Monster } from "../monster";
import { Uuid } from '../util/uuid';
import { Action, Skill, ActionDuration } from "../skill";

export class WaitSkill implements Skill {
  name = "Wait"
  desc = "Do nothing and recover some stamina."

  actionsFor = (actor: Monster, context: Battle) => {
    return [new WaitAction(actor)]
  }
}

export class WaitAction implements Action<Monster, void, Battle> {
  uuid = new Uuid()
  name = "Wait"
  desc = "Do nothing and recover some stamina."
  target = void 0
  aspects = []
  duration = new ActionDuration(5)

  constructor(public actor: Monster) {}

  perform = (bat: Battle, tick: number, duration: number) => {
    const totalRecovery = this.actor.calcEffectiveStats().con * 0.1
    const partRecovery = totalRecovery / duration
    this.actor.restoreEnergy({ stam: partRecovery }, 'Heal')
    if (tick === 0) bat.logAsMon(this.actor, "Waited around.")
  }
}