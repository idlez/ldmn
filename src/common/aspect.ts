import { LDListener, ShouldCancel } from './event';
import { Uuid } from './util/uuid';

export interface Aspect<Subject> {
  /* Mandatory descriptive properties */
  uuid: Uuid
  name: string
  desc: String
  enabled: boolean
  important: boolean
  hidden: boolean

  /* Event handlers */
  onAdding?: (subject: Subject) => ShouldCancel
  onAdded?: (subject: Subject) => void
  onEnabling?: (subject: Subject) => ShouldCancel
  onEnabled?: (subject: Subject) => void
  onRemoving?: (subject: Subject) => ShouldCancel
  onRemoved?: (subject: Subject) => void
  onDisabling?: (subject: Subject) => ShouldCancel
  onDisabled?: (subject: Subject) => void
  listeners?: LDListener<Subject>[]
}
