import {MonStats, MonFamily, MonSpecies, Energies} from '../monster'
import { Mortal } from '../aspect/mortal';
import { Adrenaline } from '../aspect/adrenaline';
import { Exhaustible } from '../aspect/exhaustible';
import { StrikeSkill } from '../skills/strike';
import { BlockSkill } from '../skills/block';
import { WaitSkill } from '../skills/wait';
import { SlashSkill } from '../skills/slash';

const CINDIGRE_BASE_STATS: MonStats = MonStats.fromI({
  con: 46,
  frt: 68,
  str: 58,
  dex: 40,
  spd: 48,
  wis: 58,
  luk: 50,
})

export const CINDIGRE_FAMILY: MonFamily = {
  name: "Cindigre",
}

export const Cindigre: MonSpecies = {
  name: "Cindigre",
  family: CINDIGRE_FAMILY,
  baseStats: CINDIGRE_BASE_STATS,
  baseResist: {},
  baseEnergy: Energies.fromStats(CINDIGRE_BASE_STATS).addAssign({
    fire: 10,
  }),
  aspects: [
    new Mortal(),
    new Exhaustible(),
    new Adrenaline(),
  ],
  assets: {
    profileImg: "./res/cindigre.png",
  },
  skills: [
    StrikeSkill,
    SlashSkill,
    new BlockSkill(),
    new WaitSkill(),
  ],
}
