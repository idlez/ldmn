import {MonStats, MonFamily, MonSpecies, Energies} from '../monster'
import { Mortal } from '../aspect/mortal';
import { Adrenaline } from '../aspect/adrenaline';
import { Exhaustible } from '../aspect/exhaustible';
import { StrikeSkill } from '../skills/strike';
import { WaitSkill } from '../skills/wait';
import { BlockSkill } from '../skills/block';

const ADARGANA_BASE_STATS: MonStats = MonStats.fromI({
  con: 46,
  frt: 68,
  str: 58,
  dex: 40,
  spd: 48,
  wis: 58,
  luk: 50,
})

export const ADARGANA_FAMILY: MonFamily = {
  name: "Adargana",
}

export const Adargana: MonSpecies = {
  name: "Adargana",
  family: ADARGANA_FAMILY,
  baseStats: ADARGANA_BASE_STATS,
  baseResist: {zap: 10},
  baseEnergy: Energies.fromStats(ADARGANA_BASE_STATS),
  aspects: [
    new Mortal(),
    new Exhaustible(),
    new Adrenaline(),
  ],
  assets: {
    profileImg: "./res/adargana.png",
  },
  skills: [
    StrikeSkill,
    new BlockSkill(),
    new WaitSkill(),
  ],
}
