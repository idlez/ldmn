
# LodeMon
> A game that is legally distinct from other turn-based monster-vs-monster RPGs.

Foolishly built with TypeScript and React. Build with Webkit via `npm run prod` to get a distributable, and load it up in your browser to play.
Theoretically it can be hosted so you don't have to compile it to play. One could even conceptualize of configuring CI to make the latest version of the game
playable at a fixed web address. As of yet, no-one has dared attempt such a feat, for fear of the madness it would bring.

Or perhaps because there isn't really much "game" to be had here. Such is a project in its infancy.

## Credits

- Monster art lifted from the [Phoenixdex].
All art in LodeMon from the Phoenixdex is licensed [CC-BY-NC 4.0]. LodeMon is not a commercial project. Thanks for licencing your art under such permissive terms, Phoenixsong.

[Phoenixdex]: http://phoenixdex.alteredorigin.net/
[CC-BY-NC 4.0]: https://creativecommons.org/licenses/by-nc/4.0/legalcode