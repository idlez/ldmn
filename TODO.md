# UI/UX
- Make it work on mobile
- Clarify which battle participant each timeline track corresponds to
- Implement (i) info clickables for keywords
  - Implement global info/warning/error modals
- Consider putting Aspect lists in scrollable containers to prevent them dominating the UI when many are present
- Some way to specialize Action details view for Attack/Status/Boost whatever type Actions

- Notify user when certain important events occur, such as the ending of a battle
  - Implement toasts/snackbars
- Polish stat boost representation
- Consider more compact representation of elemental energy pools than bars
- Polish Aspect, Monster, Tick, and Action Details UI
- Polish/finalize Monster preview w/ consistently-sized portraits, grid-based layout, and ANIMATION
- Consider potentially adding ANY SOUNDS WHATSOEVER

# Mechanics
- Cap min/max energy
- Drain energy cost when attack begins, rather than when it executes?
- Expose extendable StatBoost, etc classes that implement Action

- End a Battle when all but one participants are incapacitated - consider extensibility for when alliances exist
- Create a trainer/player DS w/ profile info, 'mon collection, items...
- Create hub/overworld data and associated UI to return to from a Battle.
- Create rooms/scenes for overworld
- Consider status effects
- Wounds/injuries